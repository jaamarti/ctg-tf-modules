# sns-topics

**Original Creator:**  Vladimir Rudyk

**Support:**  devops

**Support_email:**  vladimir.rudyk@commscope.com

Simple Notification Service (SNS) Topics
This Terraform Module creates Topics for Amazon's Simple Notification Service (SNS). The resources managed by these templates are:

An SNS topic, which can be used for messages such as CloudWatch alarm notifications. You can subscribe to this topic in the SNS Console to be notified of alarms by email or text message.
About SNS Topics
How do SNS Topics work?
Many AWS services generate various events—e.g., an EC2 Instance was launched, an ElastiCache cluster issued a fail-over—and often operators want to be notified of these events. How can we accomplish that?

One option would be for AWS to allow you to configure which emails get notified for each of the different events, but this would quickly become unmanageable since an email address would be duplicated across multiple services and it'd be hard to, for example, add a single person.

A better option is to create a "group", add one or more emails to that group, and then tell the events to notify the group. This way, we can add/remove individuals with ease without having to reconfigure multiple services. This latter option is how SNS Topics work. The SNS Topic is the "group", services "publish a message" to the SNS Topic, and individual operators can register their email as a "subscriber". In fact, you could even register an HTTP endpoint as a subscriber.

How do I get notified when a message is published to an SNS Topic?
The easiest way to get an email or text message for new SNS messages is to use the SNS Console. Click on SNS, Topics (left side of the screen), select a Topic, and select Subscribe to a Topic. Then select Email or Text Message.

## Services Created:

## Variables: 
(examples can be found in testing.tfvars or variables.tf) 
#### Required:

aws_account_id        -     The ID of the AWS Account in which to create resources
name                  -     The name of the SNS topic
display_name          -     The display name of the SNS topic
  default     = ""
} 

variable "allow_publish_accounts" {
  description = "A list of IAM ARNs that will be given the rights to publish to the SNS topic."
  type        = "list"
  default     = []

  # Example:
  # default = [
  #   "arn:aws:iam::123445678910:role/jenkins"
  # ]
}

variable "allow_subscribe_accounts" {
  description = "A list of IAM ARNs that will be given the rights to subscribe to the SNS topic."
  type        = "list"
  default     = []

  # Example:
  # default = [
  #   "arn:aws:iam::123445678910:user/jdoe"
  # ]
}


#### Optional:


variable "allow_subscribe_protocols" {
  description = "A list of protocols that can be used to subscribe to the SNS topic."
  type        = "list"

  default = [
    "http",
    "https",
    "email",
    "email-json",
    "sms",
    "sqs",
    "application",
    "lambda",
  ]
}


## pre-commit

* Install the pre-commit package

```
brew install pre-commit
```

* Install the pre-commit hook

```
pre-commit install
```

## Inspec

* Install the required ruby gems

```
bundle install
```

* Run the module to create the aws resource

```
terraform init
terraform apply
```

* Run the Inspec tests

```
terraform output --json > tests/files/terraform.json inspec exec --no-create-lockfile tests -t aws:// 
```

* Clean up test environment

```
terraform destroy
```

![Run terraform graph | dot -Tsvg > dynamodb-graph.svg](./dynamodb-graph.svg)

