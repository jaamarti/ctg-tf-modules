# ec2

**Original Creator:**  Jairo Martinez

**Support:**  devops

**Support_email:**  jairo.martinez@ctg.com

  all ec2 resources

## Services Created:

## Variables: 
(examples can be found in testing.tfvars or variables.tf) 
#### Required:


#### Optional:




## pre-commit

* Install the pre-commit package

```
brew install pre-commit
```

* Install the pre-commit hook

```
pre-commit install
```

## Inspec

* Install the required ruby gems

```
bundle install
```

* Run the module to create the aws resource

```
terraform init
terraform apply
```

* Run the Inspec tests

```
terraform output --json > tests/files/terraform.json inspec exec --no-create-lockfile tests -t aws:// 
```

* Clean up test environment

```
terraform destroy
```

![Run terraform graph | dot -Tsvg > dynamodb-graph.svg](./dynamodb-graph.svg)

