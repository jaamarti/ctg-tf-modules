package test

import (
	"testing"
//	"strings"
//	"github.com/stretchr/testify/assert"
	"github.com/gruntwork-io/terratest/modules/terraform"
 	"fmt"
    "net"
	"log"
	 "os"
	 "path/filepath"
)

func TestAlb(t *testing.T) {
	t.Parallel()


		var dir string
		dir, err := os.Getwd()
		if err != nil {
			log.Fatal(err)
		}
	
	var modules = []struct {
		modName     string
		templatePath string
		tfvarsFile   string
		withLogs     bool
	}{
		{
			"vpc",
			"../../tf-vpc/",
			"terraform.tfvars",
			false,
		},
		{
			"private",
			"../",
			"private.tfvars",
			true,
		},
		{
			"public",
			"../",
			"public.tfvars",
			true,
		},
	}

	for _, testCase := range modules {
		// The following is necessary to make sure testCase's values don't
		// get updated due to concurrency within the scope of t.Run(..) below
		testCase := testCase

       workingDir := filepath.Join(dir, testCase.templatePath)
	   tfvarfilePath := filepath.Join(workingDir, testCase.tfvarsFile)

		TerraformOptions := &terraform.Options{
			// Source path of Terraform directory.
//			TerraformDir: testCase.templatePath,
			TerraformDir: workingDir,
			Upgrade: true,
			VarFiles: []string{tfvarfilePath},
			Vars: map[string]interface{}{},             
		}

		////////////// Variables passing here //////////////////////
		// if (testCase.testName == "vpc") {
		// 	// The `alb` example requires a route53 zone
		// 	TerraformOptions.Vars["vpc_name"] = "comsearch"
		// }
		// if (testCase.modName == "alb") {
		// 	// The `alb` example requires a route53 zone
		// 	TerraformOptions.Vars["is_internal_alb"] = "false"
		// 	TerraformOptions.Vars["vpc_name"] = []string{"comsearch-ben-demo"}
		// }
		/////////////////////////////////////////////////////////////

		// This will run `terraform init` and `terraform apply` and fail the test if there are any errors
		terraform.InitAndApply(t, TerraformOptions)

		defer terraform.Destroy(t, TerraformOptions)

		//////////////// Testing sections ///////////////////////////////

        if (testCase.modName == "public") {
		albDns := terraform.Output(t, TerraformOptions, "original_alb_dns_name")
		ips, err := net.LookupIP(albDns)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Could not get IPs: %v\n", err)
		}
		for _, ip := range ips {
			fmt.Printf("google.com. IN A %s\n", ip.String())
	 	   }
		}
		///////////////////////////////////////////////////////////////////

	}

}