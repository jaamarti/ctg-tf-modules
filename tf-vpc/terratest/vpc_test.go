package test

import (
	"testing"
	"os"
	"strings"
//	"strconv"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/gruntwork-io/terratest/modules/aws"
	"github.com/stretchr/testify/assert"
	"net"
	"fmt"
	"math/big"
)

func Test(t *testing.T) {
	t.Parallel()

	terraformOptions := &terraform.Options{
		TerraformDir: "../",
		Upgrade:      true,
	}

	// 'terraform init' and 'terraform application'

	terraform.InitAndApply(t, terraformOptions)

	// run 'terraform destroy' at the end of the test
	defer terraform.Destroy(t, terraformOptions)

	// To get the value of an output variable, run 'terraform output'
    regionEnvVarName := os.Getenv("AWS_DEFAULT_REGION")
	publicSubnetCidrs := terraform.OutputList(t, terraformOptions, "public_subnet_cidr_blocks")
	privateAppSubnetCidrs := terraform.OutputList(t, terraformOptions, "private_app_subnet_cidr_blocks")
	privatePersistenceSubnetCidrs := terraform.OutputList(t, terraformOptions, "private_app_subnet_cidr_blocks")
	publicSubnetIds := terraform.OutputList(t, terraformOptions, "public_subnet_ids")
	privateAppSubnetIds := terraform.OutputList(t, terraformOptions, "private_app_subnet_ids")
	privatePersistenceSubnetIds := terraform.OutputList(t, terraformOptions, "private_persistence_subnet_ids")
	publicSubnetRouteTableId := terraform.Output(t, terraformOptions, "public_subnet_route_table_id") 
	privateAppSubnetRouteTableId := terraform.OutputList(t, terraformOptions, "private_app_subnet_route_table_ids")
	privatePesistenceSubnetRouteTableId := terraform.OutputList(t, terraformOptions, "private_persistence_route_table_ids")     
	natGwPublicIps := terraform.OutputList(t, terraformOptions, "nat_gateway_public_ips")
	natGwIds := terraform.OutputList(t, terraformOptions, "nat_gateway_ids")
	numAzs := terraform.OutputList(t, terraformOptions, "availability_zones")
	vpcName := terraform.Output(t, terraformOptions, "vpc_name")
	vpcEnv := terraform.Output(t, terraformOptions, "vpc_env")
	vpcId := terraform.Output(t, terraformOptions, "vpc_id")
	vpcCidr := terraform.Output(t, terraformOptions, "vpc_cidr_block")


    TotalSubnets := aws.GetSubnetsForVpc(t, vpcId, regionEnvVarName)

	//Expected Values

	//expectedPublicSubnetCidrs := []string{"10.0.0.0/28", "10.0.0.16/28", "10.0.0.32/28"}
	//CIDRList := []string{"10.0.0.0/28", "10.0.0.16/28", "10.0.0.32/28"}
	//CIDRBlock := string("10.0.0.0/23")
	//expectedPrivateAppSubnetCidrs := []string{"10.0.0.160/28", "10.0.0.176/28", "10.0.0.192/28"}
	//expectedPrivatePersistenceSubnetCidrs := []string{"10.0.0.160/28", "10.0.0.176/28", "10.0.0.192/28"}
	expectedVpcName := string("comsearch")
	expectedVpcEnv := string("development")
	expectedNatGwPublicIps := int(1)
	expectedNatGwIds := int(1)
	expectePrivateAppSubnetRouteTableId := int(3)
	expectePrivatePesistenceSubnetRouteTableId := int(3)
	expectePublicSubnetIds := int(3)
	expectePrivateAppSubnetIds := int(3)
	expectePrivatePersistenceSubnetIds := int(3)

	// Check the vpc subnets CIDR list and the main vpc CIDR (CIDRBlock) and verify if none of the CIDR subnets are overlaping and if all CIDR subnets are in the main VPC CIDR network

		_, CIDRBlock, perr := net.ParseCIDR(vpcCidr)
		if perr != nil {
			t.Errorf("Bad test data %s\n", CIDRBlock)
		}

		subnets := make([]*net.IPNet, len(publicSubnetCidrs))
		for i, s := range publicSubnetCidrs {
			_, n, err := net.ParseCIDR(s)
			if err != nil {
				t.Errorf("Bad test data %s\n", s)
			}
			subnets[i] = n
		}

		test := VerifyNotOverlaping(subnets, CIDRBlock)
		if test != nil {
			t.Errorf("Failed test with %v\n", test)
		}
	
	//privateAppSubnetCidrs

	subnetsApp := make([]*net.IPNet, len(privateAppSubnetCidrs))
	for i, s := range privateAppSubnetCidrs {
		_, n, err := net.ParseCIDR(s)
		if err != nil {
			t.Errorf("Bad test data %s\n", s)
		}
		subnetsApp[i] = n
	}

	test1 := VerifyNotOverlaping(subnetsApp, CIDRBlock)
	if test1 != nil {
		t.Errorf("Failed test with %v\n", test1)
	}
	//privatePersistenceSubnetCidrs

	subnetsPers := make([]*net.IPNet, len(privatePersistenceSubnetCidrs))
	for i, s := range privatePersistenceSubnetCidrs {
		_, n, err := net.ParseCIDR(s)
		if err != nil {
			t.Errorf("Bad test data %s\n", s)
		}
		subnetsPers[i] = n
	}

	test2 := VerifyNotOverlaping(subnetsPers, CIDRBlock)
	if test2 != nil {
		t.Errorf("Failed test with %v\n", test2)
	}

	 for _, value := range publicSubnetIds {
	 	assert.True(t, aws.IsPublicSubnet(t, value, regionEnvVarName))
	   }
	
     for _, value := range privateAppSubnetIds {
	 	assert.False(t, aws.IsPublicSubnet(t, value, regionEnvVarName))
	   }

	   for _, value := range privatePersistenceSubnetIds {
		assert.False(t, aws.IsPublicSubnet(t, value, regionEnvVarName))
	  }


	// Check that we get back the outputs that we expect

	assert.Equal(t, expectedVpcName, vpcName)
	assert.Equal(t, expectedVpcEnv, vpcEnv)
//	assert.Equal(t, expectedPublicSubnetCidrs, publicSubnetCidrs)
//	assert.Equal(t, expectedPrivateAppSubnetCidrs, privateAppSubnetCidrs)
//	assert.Equal(t, expectedPrivatePersistenceSubnetCidrs, privatePersistenceSubnetCidrs)
	assert.Regexp(t, "^rtb-[[:alnum:]]+$", publicSubnetRouteTableId)
	assert.Equal(t, len(privateAppSubnetRouteTableId), expectePrivateAppSubnetRouteTableId)
	assert.Equal(t, len(privatePesistenceSubnetRouteTableId), expectePrivatePesistenceSubnetRouteTableId)
	assert.True(t, len(numAzs) > 1)
	assert.Equal(t, len(natGwPublicIps), expectedNatGwPublicIps)
	assert.Equal(t, len(natGwIds), expectedNatGwIds)
	assert.Equal(t, len(publicSubnetIds), expectePublicSubnetIds)
	assert.Equal(t, len(privateAppSubnetIds), expectePrivateAppSubnetIds)
	assert.Equal(t, len(privatePersistenceSubnetIds), expectePrivatePersistenceSubnetIds)
	assert.True(t, len(TotalSubnets) > 8)
}
	
func GetFirstTwoOctets(cidrBlock string) string {
	ipAddr := strings.Split(cidrBlock, "/")[0]
	octets := strings.Split(ipAddr, ".")
	return octets[0] + "." + octets[1]
}

func VerifyNotOverlaping(subnets []*net.IPNet, CIDRBlock *net.IPNet) error {
	firstLastIP := make([][]net.IP, len(subnets))
	for i, s := range subnets {
		first, last := AddressRange(s)
		firstLastIP[i] = []net.IP{first, last}
	}
	for i, s := range subnets {
		if !CIDRBlock.Contains(firstLastIP[i][0]) || !CIDRBlock.Contains(firstLastIP[i][1]) {
			return fmt.Errorf("%s does not fully contain %s", CIDRBlock.String(), s.String())
		}
		for j := 0; j < len(subnets); j++ {
			if i == j {
				continue
			}

			first := firstLastIP[j][0]
			last := firstLastIP[j][1]
			if s.Contains(first) || s.Contains(last) {
				return fmt.Errorf("%s overlaps with %s", subnets[j].String(), s.String())
			}
		}
	}
	return nil
}

func AddressRange(network *net.IPNet) (net.IP, net.IP) {
	// the first IP is easy
	firstIP := network.IP

	// the last IP is the network address OR NOT the mask address
	prefixLen, bits := network.Mask.Size()
	if prefixLen == bits {
		// Easy!
		// But make sure that our two slices are distinct, since they
		// would be in all other cases.
		lastIP := make([]byte, len(firstIP))
		copy(lastIP, firstIP)
		return firstIP, lastIP
	}

	firstIPInt, bits := ipToInt(firstIP)
	hostLen := uint(bits) - uint(prefixLen)
	lastIPInt := big.NewInt(1)
	lastIPInt.Lsh(lastIPInt, hostLen)
	lastIPInt.Sub(lastIPInt, big.NewInt(1))
	lastIPInt.Or(lastIPInt, firstIPInt)

	return firstIP, intToIP(lastIPInt, bits)
}

func ipToInt(ip net.IP) (*big.Int, int) {
	val := &big.Int{}
	val.SetBytes([]byte(ip))
	if len(ip) == net.IPv4len {
		return val, 32
	} else if len(ip) == net.IPv6len {
		return val, 128
	} else {
		panic(fmt.Errorf("Unsupported address length %d", len(ip)))
	}
}

func intToIP(ipInt *big.Int, bits int) net.IP {
	ipBytes := ipInt.Bytes()
	ret := make([]byte, bits/8)
	// Pack our IP bytes into the end of the return array,
	// since big.Int.Bytes() removes front zero padding.
	for i := 1; i <= len(ipBytes); i++ {
		ret[len(ret)-i] = ipBytes[len(ipBytes)-i]
	}
	return net.IP(ret)
}