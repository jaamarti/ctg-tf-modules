alb_name        =       "comsearch-alb-private"
is_internal_alb    =       "true"
vpc_name        =       ["comsearch"]
domain_name     =       "alb-dns"
num_days_after_which_archive_log_data = 0
num_days_after_which_delete_log_data = 0
