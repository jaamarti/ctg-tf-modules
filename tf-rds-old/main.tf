provider "aws" {
  # region = var.aws_region
}

# terraform {
#   backend "s3" {}
# }

data "aws_vpc" "selected" {
  filter {
    name   = "tag:Name"
    values = var.vpc_name
  }
}

data "aws_subnet_ids" "private_app" {
  vpc_id = data.aws_vpc.selected.id
  filter {
    name   = "tag:Name"
    values = ["*-private-app-*"]
  }
}

data "aws_subnet_ids" "private_db" {
  vpc_id = data.aws_vpc.selected.id
  filter {
    name   = "tag:Name"
    values = ["*-private-persistence-*"]
  }
}


data "aws_subnet" "private_app" {
  for_each = data.aws_subnet_ids.private_app.ids
  id       = each.value
}

module "database" {
  source = "git::ssh://git@bitbucket.org/lego-comsearch/module-data-storage.git//modules/rds?ref=v0.20.0"

  name                = var.name
  db_name             = var.db_name
  engine              = var.engine
  engine_version      = var.engine_version
  port                = var.port
  license_model       = var.license_model
  snapshot_identifier = var.snapshot_identifier
  skip_final_snapshot = var.skip_final_snapshot
  master_username     = var.master_username
  master_password     = var.master_password

  create_subnet_group                = true
  vpc_id                             = data.aws_vpc.selected.id
  #subnet_ids                         = var.private_persistent_subnet_ids
  subnet_ids =  data.aws_subnet_ids.private_db.ids
  # allow_connections_from_cidr_blocks = var.allow_connections_from_cidr_blocks
  allow_connections_from_cidr_blocks = "${values(data.aws_subnet.private_app).*.cidr_block}"
  allow_connections_from_security_groups = [] 

  instance_type     = var.instance_type
  allocated_storage = var.allocated_storage
  num_read_replicas = var.num_read_replicas

  storage_encrypted = var.storage_encrypted
  kms_key_arn       = "" 
  #data.terraform_remote_state.kms_master_key.outputs.key_arn

  multi_az                = var.multi_az
  backup_retention_period = var.backup_retention_period
  apply_immediately       = var.apply_immediately
}

resource "aws_ssm_parameter" "db_uri" {
  name  = "${var.ssm_parameter_path}/JDBC_URL"
  type  = "String"
  value = "${module.database.primary_endpoint}/${var.db_name}"
  overwrite = true
}

resource "aws_ssm_parameter" "db_host" {
  name  = "${var.ssm_parameter_path}/DB_HOST"
  type  = "String"
  value = element(split(":", module.database.primary_endpoint), 0)
  overwrite = true
}

resource "aws_ssm_parameter" "db_name" {
  name  = "${var.ssm_parameter_path}/DB_NAME"
  type  = "String"
  value = var.name
  overwrite = true
}

resource "aws_ssm_parameter" "db_port" {
  name  = "${var.ssm_parameter_path}/DB_PORT"
  type  = "String"
  value = module.database.port
  overwrite = true
}