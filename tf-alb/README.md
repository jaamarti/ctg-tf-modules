# alb

**Original Creator:**  Vladimir Rudyk

**Support:**  devops

**Support_email:**  vladimir.rudyk@commscope.com

Application Load Balancer (ALB)
This Terraform Module creates an Application Load Balancer (ALB) that can be used to route requests to an ECS Cluster or Auto Scaling Group. Under the hood, this is implemented using the Gruntwork alb module.

Note that a single ALB is designed to be shared among multiple ECS Clusters, ECS Services or Auto Scaling Groups, in contrast to an ELB ("Classic Load Balancer") which is typically associated with a single service. For this reason, the ALB is created separately from an ECS Cluster, ECS Service, or Auto Scaling Group.

How do you use this module?
See the root README for instructions on using modules.

Core concepts
To understand core concepts like what is an ALB, ELB vs. ALB, and how to use an ALB with an ECS Cluster and ECS Service, the alb module documentation.

## Services Created:

## Variables: 
(examples can be found in testing.tfvars or variables.tf) 
#### Required:


#### Optional:




## pre-commit

* Install the pre-commit package

```
brew install pre-commit
```

* Install the pre-commit hook

```
pre-commit install
```

## Inspec

* Install the required ruby gems

```
bundle install
```

* Run the module to create the aws resource

```
terraform init
terraform apply
```

* Run the Inspec tests

```
terraform output --json > tests/files/terraform.json inspec exec --no-create-lockfile tests -t aws:// 
```

* Clean up test environment

```
terraform destroy
```

![Run terraform graph | dot -Tsvg > dynamodb-graph.svg](./dynamodb-graph.svg)

