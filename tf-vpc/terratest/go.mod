module service

go 1.16

require (
	github.com/aws/aws-sdk-go v1.38.64 // indirect
	github.com/aws/aws-sdk-go-v2 v1.6.0 // indirect
	github.com/aws/aws-sdk-go-v2/config v1.3.0 // indirect
	github.com/aws/aws-sdk-go-v2/service/s3 v1.10.0 // indirect
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/google/uuid v1.1.1 // indirect
	github.com/gruntwork-io/go-commons v0.8.0 // indirect
	github.com/gruntwork-io/terratest v0.36.1 // indirect
	github.com/hashicorp/go-multierror v1.1.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/pquerna/otp v1.2.0 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/tools v0.1.3 // indirect
)
