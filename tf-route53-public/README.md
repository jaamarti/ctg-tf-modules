# route53-public

**Original Creator:**  Vladimir Rudyk

**Support:**  devops

**Support_email:**  vladimir.rudyk@commscope.com

Route 53 Public Hosted Zones
This Terraform Modules manages public DNS entries using Amazon Route 53.

For each domain name (e.g. example.com) you pass in, this module will create a Route 53 Public Hosted Zone. The Terraform configurations for each app are responsible for adding their individual DNS records (e.g. foo.example.com) to this Hosted Zone.

How do you use this module?
See the root README for instructions on using modules.

Core concepts
To understand core concepts like what is route 53, what is a public hosted zone, and more, see the route 53 documentation.

## Services Created:

## Variables: 
(examples can be found in testing.tfvars or variables.tf) 
#### Required:

primary_domain_name    -   The primary domain name (e.g. foo.com) for which to create a Route 53 Public Hosted Zone


#### Optional:

primary_domain_name_comment     -   A comment or description of the domain name in var.primary_domain_name  (default     = "Hosted zone managed by Terraform)


## pre-commit

* Install the pre-commit package

```
brew install pre-commit
```

* Install the pre-commit hook

```
pre-commit install
```

## Terratest


1. Verify you have the latest version of Go

2. Change into terratest dir with command:

cd terratest

3. Then run command:

go test -v -timeout 45m

4. For run an specific fuction can use:

go test -v -run <function_name_here>
