output "master_id" {
  value = aws_db_instance.master.id
}

output "master_arn" {
  value = aws_db_instance.master.arn
}

output "master_endpoint" {
  value = aws_db_instance.master.endpoint
}

# The master_endpoint is of the format <host>:<port>. This output returns just the host part.
output "master_host" {
  value = element(split(":", aws_db_instance.master.endpoint), 0)
}

output "port" {
  value = var.port
}

output "name" {
  value = var.name
}

output "db_name" {
  value = aws_db_instance.master.name
}

output "db_subnet_ids" {
  value = "${data.aws_subnet_ids.private_db.ids}"
}

output "skip_final_snapshot" {
  value = "${var.skip_final_snapshot}"
}

# output "secret_db_name" {
#   value = aws_secretsmanager_secret.db_credentials.name
# }