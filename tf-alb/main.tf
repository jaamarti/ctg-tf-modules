provider "aws" {
  # The AWS region in which all resources will be created
  # region = "${var.aws_region}"
}

data "aws_vpc" "selected" {
  filter {
    name   = "tag:Name"
    values = var.vpc_name
  }
}

data "aws_subnet_ids" "private" {
  vpc_id = data.aws_vpc.selected.id
  filter {
    name   = "tag:Name"
    values = ["*-private-app-*"]
  }
}

data "aws_subnet_ids" "public" {
  vpc_id = data.aws_vpc.selected.id
  filter {
    name   = "tag:Name"
    values = ["*-public-*"]
  }
}

# data "aws_route53_zone" "private" {
#   name         = "comsearch-development-ctg.com"
#   private_zone = true
# }

# data "aws_route53_zone" "public" {
#   name         = "comsearch-development-public.com"
#   private_zone = false
# }

module "alb" {
  source = "git::ssh://git@bitbucket.org/lego-comsearch/module-load-balancer.git//modules/alb?ref=v0.21.0"

  # You can find the list of policies here: https://docs.aws.amazon.com/elasticloadbalancing/latest/application/create-https-listener.html#describe-ssl-policies
  ssl_policy = "ELBSecurityPolicy-2016-08"

  alb_name        = var.alb_name
  is_internal_alb = var.is_internal_alb

  http_listener_ports = var.http_listener_ports

  https_listener_ports_and_ssl_certs     = var.https_listener_ports_and_ssl_certs

  https_listener_ports_and_acm_ssl_certs     = var.https_listener_ports_and_acm_ssl_certs

  allow_inbound_from_cidr_blocks = var.allow_inbound_from_cidr_blocks


  allow_inbound_from_security_group_ids = []
  # data.terraform_remote_state.bastion_host.outputs.bastion_host_security_group_id
  allow_inbound_from_security_group_ids_num = 0
  # 1

  vpc_id                        = data.aws_vpc.selected.id
  additional_security_group_ids = var.additional_security_group_ids

  vpc_subnet_ids = var.is_internal_alb ? data.aws_subnet_ids.private.ids : data.aws_subnet_ids.public.ids


  # VR: no need for access logs
  enable_alb_access_logs         = true
  alb_access_logs_s3_bucket_name = module.alb_access_logs_bucket.s3_bucket_name


  allow_all_outbound         = var.allow_all_outbound
  idle_timeout               = var.idle_timeout
  enable_deletion_protection = var.enable_deletion_protection
  drop_invalid_header_fields = var.drop_invalid_header_fields

  default_action_content_type = var.default_action_content_type
  default_action_body         = var.default_action_body
  default_action_status_code  = var.default_action_status_code
}

resource "aws_ssm_parameter" "terrain_host" {
  count     = var.is_internal_alb ? 1 : 0
  name      = "${var.ssm_parameter_path}/COMSEARCH_TERRAIN_HOST"
  type      = "String"
  value     = module.alb.alb_dns_name
  overwrite = true
}

resource "aws_ssm_parameter" "terrain_port" {
  count     = var.is_internal_alb ? 1 : 0
  name      = "${var.ssm_parameter_path}/COMSEARCH_TERRAIN_PORT"
  type      = "String"
  value     = "8080"
  overwrite = true
}

resource "aws_ssm_parameter" "analysis_host" {
  count     = var.is_internal_alb ? 1 : 0
  name      = "${var.ssm_parameter_path}/COMSEARCH_ANALYSIS_HOST"
  type      = "String"
  value     = module.alb.alb_dns_name
  overwrite = true
}

resource "aws_ssm_parameter" "analysis_port" {
  count     = var.is_internal_alb ? 1 : 0
  name      = "${var.ssm_parameter_path}/COMSEARCH_ANALYSIS_PORT"
  type      = "String"
  value     = "8080"
  overwrite = true
}

module "alb_access_logs_bucket" {
  source = "git::ssh://git@bitbucket.org/lego-comsearch/module-aws-monitoring.git//modules/logs/load-balancer-access-logs?ref=v0.21.0"

  # s3_bucket_name = length(var.access_logs_s3_bucket_name) > 0 ? var.access_logs_s3_bucket_name : "alb-${lower(replace(var.alb_name, "_", "-"))}-access-logs"
  s3_bucket_name = "${lower(replace(var.alb_name, "_", "-"))}-access-logs"

  #s3_logging_prefix = var.is_internal_alb == false ? "${var.alb_name}-public" : "${var.alb_name}-private"
  s3_logging_prefix = var.alb_name
  force_destroy = true

  num_days_after_which_archive_log_data = var.num_days_after_which_archive_log_data
  num_days_after_which_delete_log_data  = var.num_days_after_which_delete_log_data
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE A DNS RECORD USING ROUTE 53
# ---------------------------------------------------------------------------------------------------------------------

# # The ECS Service's endpoint will point to the ELB.
# resource "aws_route53_record" "dns_record" {
# //count = 1

#   for_each = toset( [data.aws_route53_zone.private.zone_id, data.aws_route53_zone.public.zone_id] )

# //zone_id = element(concat("${data.aws_route53_zone.private.zone_id}","${data.aws_route53_zone.public.zone_id}"), 0)
#  //zone_id = data.aws_route53_zone.private.zone_id
#  //zone_id = data.aws_route53_zone.public.zone_id
#   zone_id = each.key

#   //for_each = "${setunion(toset(data.aws_route53_zone.private.zone_id), toset(data.aws_route53_zone.public.zone_id))}"
#   //zone_id = each.value

#   name    = var.domain_name
#   type    = "A"

#   alias {
#     name                   = module.alb.alb_dns_name
#     zone_id                = module.alb.alb_hosted_zone_id
#     evaluate_target_health = true
#   }
# }

# ---------------------------------------------------------------------------------------------------------------------
# PULL DATA FROM OTHER TERRAFORM TEMPLATES USING TERRAFORM REMOTE STATE
# store their state in S3 buckets.
# ---------------------------------------------------------------------------------------------------------------------

#data "aws_vpc" "selected" {
#  filter {
#    name   = "tag:Name"
#    values = ["lego-best"]
#  }
#}
#
#data "aws_subnet_ids" "public" {
#  vpc_id =  data.aws_vpc.selected.id
#
#  filter {
#    name   = "tag:Name"
#    values = ["*private-app*"]
#  }
#}
#
#data "aws_subnet" "public" {
#  for_each = data.aws_subnet_ids.public.ids
#  id       = each.value
#}
#
#
##data "terraform_remote_state" "vpc" {
#data "aws_subnet_ids" "private" {
#  vpc_id =  data.aws_vpc.selected.id
#
#  filter {
#    name   = "tag:Name"
#    values = ["*private-app*"]
#  }
#}
#
#data "aws_subnet" "private" {
#  for_each = data.aws_subnet_ids.public.ids
#  id       = each.value
#}


#data "terraform_remote_state" "vpc" {
#  backend = "s3"
#
#  config = {
#    region = var.terraform_state_aws_region
#    bucket = var.terraform_state_s3_bucket
#    key    = "${var.aws_region}/${var.environment}/vpc-app/terraform.tfstate"
#  }
#}

# data "terraform_remote_state" "bastion_host" {
#   backend = "s3"

#   config = {
#     region = var.terraform_state_aws_region
#     bucket = var.terraform_state_s3_bucket
#     key    = "${var.aws_region}/mgmt/bastion-host/terraform.tfstate"
#   }
# }

# data "terraform_remote_state" "route53_private" {
#   count = "${var.create_route53_entry * var.is_internal_alb}"

#   backend = "s3"

#   config = {
#     region = var.terraform_state_aws_region
#     bucket = var.terraform_state_s3_bucket
#     key    = "${var.aws_region}/${var.environment}/networking/route53-private/terraform.tfstate"
#   }
# }

# data "terraform_remote_state" "route53_public" {
#   count = "${var.create_route53_entry * (1 - var.is_internal_alb)}"

#   backend = "s3"

#   config = {
#     region = var.terraform_state_aws_region
#     bucket = var.terraform_state_s3_bucket
#     key    = "_global/route53-public/terraform.tfstate"
#   }
# }