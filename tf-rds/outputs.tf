output "primary_id" {
  value = aws_db_instance.primary.id
}

output "primary_arn" {
  value = aws_db_instance.primary.arn
}

output "primary_endpoint" {
  value = aws_db_instance.primary.endpoint
}

output "num_read_replicas" {
  value = var.num_read_replicas
}

# These will only show up if you set num_read_replicas > 0
output "read_replica_ids" {
  value = aws_db_instance.replicas[*].id
}

# These will only show up if you set num_read_replicas > 0
output "read_replica_arns" {
  value = aws_db_instance.replicas[*].arn
}

# These will only show up if you set num_read_replicas > 0
output "read_replica_endpoints" {
  value = aws_db_instance.replicas[*].endpoint
}

# The primary_endpoint is of the format <host>:<port>. This output returns just the host part.
output "primary_host" {
  value = element(split(":", aws_db_instance.primary.endpoint), 0)
}

output "port" {
  value = var.port
}

output "name" {
  value = var.name
}

output "db_name" {
  value = aws_db_instance.primary.name
}

output "db_subnet_ids" {
  value = "${data.aws_subnet_ids.private_db.ids}"
}

output "skip_final_snapshot" {
  value = "${var.skip_final_snapshot}"
}

# output "secret_db_name" {
#   value = aws_secretsmanager_secret.db_credentials.name
# }