
output "vpc_id" {
  value = aws_vpc.main.id
}

output "vpc_name" {
  value = var.vpc_name
}

output "vpc_cidr_block" {
  value = aws_vpc.main.cidr_block
}

// A VPC automatically comes with a default security group. If you don't
// specify a different security group when you launch an instance, AWS 
// associates the default security group with your instance. You can't
// delete a default security group.
output "default_security_group_id" {
  value = aws_vpc.main.default_security_group_id
}

output "public_subnet_cidr_blocks" {
  value = aws_subnet.public[*].cidr_block
}

output "private_app_subnet_cidr_blocks" {
  value = aws_subnet.private-app[*].cidr_block
}

output "private_persistence_subnet_cidr_blocks" {
  value = aws_subnet.private-persistence[*].cidr_block
}

output "public_subnet_ids" {
  value = aws_subnet.public[*].id
}

output "private_app_subnet_ids" {
  value = aws_subnet.private-app[*].id
}

output "private_persistence_subnet_ids" {
  value = aws_subnet.private-persistence[*].id
}

output "public_subnet_arns" {
  value = aws_subnet.public[*].arn
}

output "private_app_subnet_arns" {
  value = aws_subnet.private-app[*].arn
}

output "private_persistence_subnet_arn" {
  value = aws_subnet.private-persistence[*].arn
}

output "public_subnet_route_table_id" {
  value = var.create_public_subnets ? aws_route_table.public[0].id : null
}

output "private_app_subnet_route_table_ids" {
  value = aws_route_table.private-app[*].id
}

// This output is identical to the private_persistence_subnet_route_table_ids output below. This one does not follow
// our naming conventions, but we keep it around to maintain backwards compatibility.
output "private_persistence_route_table_ids" {
  value = aws_route_table.private-persistence[*].id
}

output "private_persistence_subnet_route_table_ids" {
  value = aws_route_table.private-persistence[*].id
}

output "nat_gateway_ids" {
  value = aws_nat_gateway.nat[*].id
}

output "nat_gateway_public_ips" {
  value = aws_eip.nat[*].public_ip
}

output "num_availability_zones" {
  value = data.template_file.num_availability_zones.rendered
}

output "availability_zones" {
  value = slice(data.aws_availability_zones.all.names, 0, data.template_file.num_availability_zones.rendered)
}

output "vpc_ready" {
  value = null_resource.vpc_ready.id
}

output "vpc_env" {
  value = lookup(aws_vpc.main.tags, "Environment")
}
