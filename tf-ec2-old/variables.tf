# Module: ec2

variable "aws_region" {
  description   = "Region defined"
  default = "us-east-2"
}

variable "env" {
  description   = "Enviroment defined for deployment"
  default       = "dev"
}

variable "vpcid" {
  description   = "Region defined"
  default            = "data.aws_vpc.selected.id"
}


## Launch template variables 


variable "launcht_name" {
  description   = "launch template prefix name"
  default       = "asg-priv-template"
}

variable "ami" {
  description   = "Last ami XXX defined in launch template"
  default       = "ami-00399ec92321828f5"
}

variable "instance_type" {
  description   = "Instance type defined in ASG"
  default       = "t2.micro"
}

## ASG Variables

variable "asg_name" {
  description   = "Private ASG Name"
  default       = "asg-priv-template"
}

variable "desired_cap" {
  description   = "ASG desired capacity"
  default       = "1"
}

variable "max_cap" {
  description   = "ASG Max capacity"
  default       = "1"
}
variable "min_cap" {
  description   = "ASG Min capacity"
  default       = "1"
}
variable "billing" {
  description   = "Billing description"
  default       = "ConnectXX"
}
variable "cont_serv" {
  description   = "Container service"
  default       = "/PATH/HERE"
}
variable "ssm_parameter_path" {
  description   = "SSM Parameter store path"
  default       = "/connect-058ef9685456c87f6"
}
variable "service_name" {
  description   = "ASG EC2 instances name"
  default       = "instances front/back (asg)"
}
variable "stack" {
  description   = "stack id"
  default       = "058ef9685456c87f6"
}

