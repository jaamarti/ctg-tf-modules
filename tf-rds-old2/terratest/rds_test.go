package test

import (
	"testing"
	"github.com/gruntwork-io/terratest/modules/terraform"
 	"fmt"
    "net"
	"log"
	 "os"
	 "path/filepath"
	//  "strings"

	 "github.com/gruntwork-io/terratest/modules/aws"
	//  "github.com/gruntwork-io/terratest/modules/random"
	 "github.com/stretchr/testify/assert"
)

func TestRds(t *testing.T) {
	t.Parallel()


		var dir string
		dir, err := os.Getwd()
		if err != nil {
			log.Fatal(err)
		}
	
	var modules = []struct {
		modName     string
		templatePath string
		tfvarsFile   string
		withLogs     bool
	}{
		// {
		// 	"vpc",
		// 	"../../tf-vpc/",
		// 	"terraform.tfvars",
		// 	false,
		// },
		{
			"rds",
			"../",
			"terraform.tfvars",
			true,
		},
	}

	for _, testCase := range modules {

		testCase := testCase

        workingDir := filepath.Join(dir, testCase.templatePath)
	    tfvarfilePath := filepath.Join(workingDir, testCase.tfvarsFile)
		awsRegion := os.Getenv("AWS_DEFAULT_REGION")

		TerraformOptions := &terraform.Options{
			// Source path of Terraform directory.
//			TerraformDir: testCase.templatePath,
			TerraformDir: workingDir,
			Upgrade: true,
			VarFiles: []string{tfvarfilePath},
			Vars: map[string]interface{}{},             
		}
      
		////////////// Variables passing here //////////////////////
		if (testCase.modName == "rds") {

			TerraformOptions.Vars["skip_final_snapshot"] = "true"
		}
		// if (testCase.modName == "alb") {
		// 	// The `alb` example requires a route53 zone
		// 	TerraformOptions.Vars["is_internal_alb"] = "false"
		// 	TerraformOptions.Vars["vpc_name"] = []string{"comsearch-ben-demo"}
		// }
		/////////////////////////////////////////////////////////////

		terraform.InitAndApply(t, TerraformOptions)

		//defer terraform.Destroy(t, TerraformOptions)

		//////////////// Testing sections ///////////////////////////////

		if (testCase.modName == "rds") {
			dbInstanceId := terraform.Output(t, TerraformOptions, "primary_id")
			dbSubnetIds := terraform.OutputList(t, TerraformOptions, "db_subnet_ids")
			dbInstanceEndpoint := terraform.Output(t, TerraformOptions, "primary_endpoint")
			dbParams := aws.GetAllParametersOfRdsInstance(t, dbInstanceId, awsRegion)
			// address := aws.GetAddressOfRdsInstance(t, dbInstanceID, awsRegion)
			// port := aws.GetPortOfRdsInstance(t, dbInstanceID, awsRegion)
			// schemaExistsInRdsInstance := aws.GetWhetherSchemaExistsInRdsMySqlInstance(t, address, port, username, password, expectedDatabaseName)
		    rdsOptionGroup := aws.GetOptionGroupNameOfRdsInstance(t, dbInstanceId, awsRegion)		
			// fmt.Println(rdsOptionGroup)
			assert.True(t, len(dbParams) > 1)
			assert.Equal(t, dbInstanceId, "dev-db")
			assert.NotNil(t, rdsOptionGroup)

			//  Test tcp connetion to db instance
			conn, err := net.Dial("tcp", dbInstanceEndpoint)
			if err == nil {
				fmt.Println(conn)
				//conn := string("")
			}
			fmt.Println(err)
			assert.NotNil(t, err)
			
			// check if db are place in private subnet
			for _, value := range dbSubnetIds {
				assert.False(t, aws.IsPublicSubnet(t, value, awsRegion))
			  }
	
		}
		///////////////////////////////////////////////////////////////////

	}

}