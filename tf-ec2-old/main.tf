# Module: ec2

data "aws_vpc" "selected" {
  filter {
    name   = "tag:Name"
    values = ["jmdev-vpc"]
  }
}

data "aws_subnet_ids" "selected" {
  vpc_id = data.aws_vpc.selected.id
  filter {
    name   = "tag:Name"
    values = ["*-private-*"]
  }
}

data "aws_ami" "asg_ami" {
  most_recent      = true
  name_regex       = ".*amzn2-ami-hvm-2.0.20210701.0-arm64-gp2.*"
  owners = ["amazon"]
}

resource "aws_security_group" "template_priv_sg" {
  name        = "sg_alb"
  description = "demo desc"
  vpc_id      = data.aws_vpc.selected.id
}

resource "aws_launch_template" "asg_priv_template" {
  name_prefix   = var.launcht_name
  image_id      = data.aws_ami.asg_ami.id
  instance_type = var.instance_type
  vpc_security_group_ids = ["${aws_security_group.template_priv_sg.id}"]
}

# resource "aws_autoscaling_group" "asg_template" {
#   name                 = var.asg_name
#   vpc_zone_identifier  = data.aws_subnet_ids.selected.ids  
#   desired_capacity   = var.desired_cap
#   max_size           = var.max_cap
#   min_size           = var.min_cap
#   target_group_arns  = [aws_lb_target_group.alb_priv_listener_tg.arn]

#   tags = [
#     {
#       key                 = "Billing"
#       value               = var.billing
#       propagate_at_launch = true
#     },
#     {
#       key                 = "Environment"
#       value               = var.env
#       propagate_at_launch = true
#     },
#     {
#       key                 = "container.service"
#       value               = var.cont_serv
#       propagate_at_launch = true
#     },
#     {
#       key                 = "ssm.parameter_path"
#       value               = var.ssm_parameter_path
#       propagate_at_launch = true
#     },
#     {
#       key                 = "stack"
#       value               = var.stack
#       propagate_at_launch = true
#     },
#     {
#       key                 = "Name"
#       value               = var.service_name
#       propagate_at_launch = true
#     },
#   ]
# }

resource "aws_lb" "alb_private" {
  name               = "alb-private"
  internal           = true
  load_balancer_type = "application"
  security_groups    = [aws_security_group.template_priv_sg.id]
  subnets            = data.aws_subnet_ids.selected.ids

  enable_deletion_protection = false

#  access_logs {
#    bucket  = aws_s3_bucket.lb_logs.bucket
#    prefix  = "test-lb"
#    enabled = false
#  }

  tags = {
    Environment = "var.env"
    Billing = "var.billing"
  }
}

# resource "aws_lb_target_group" "alb_priv_listener_tg" {
#   name     = "alb-priv-listener-tg"
#   port     = 80
#   protocol = "HTTP"
#   vpc_id   = data.aws_vpc.selected.id
# }

resource "aws_lb_listener" "alb_private_listener" {
  load_balancer_arn = aws_lb.alb_private.arn
  port              = "80"
  protocol          = "HTTP"
#  ssl_policy        = "ssl policy"
#  certificate_arn   = "arn cert"

  # condition {
  #   path_pattern {
  #     values = ["/static/*"]
  #   }
  # }

  # condition {
  #   host_header {
  #     values = ["aws_lb.alb_private.dns_name"]
  #   }
  # }

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "bad request. It is default result"
      status_code  = "500"
    }
  }
}

# resource "aws_lb_listener_rule" "static" {
#   listener_arn = aws_lb_listener.alb_private_listener.arn
#   priority     = 100

#   action {
#     type             = "forward"
#     target_group_arn = aws_lb_target_group.alb_priv_listener_tg.arn
#   }

#   condition {
#     path_pattern {
#       values = ["/static/*"]
#     }
#   }

#   condition {
#     host_header {
#       values = [aws_lb.alb_private.dns_name]
#     }
#   }
# }
