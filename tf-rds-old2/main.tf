locals {

  monitoring_role_name          = join("-", [var.stack, var.name, "Monitoring_role"])
  instance_name                 = join("-", [var.stack, "rds", var.name])  
  db_subnet_group_name          = join("-", [var.stack, var.name, "rds", "subnet_group"])
  db_subnet_group_description   = "Subnet group for the ${var.name} DB"
  final_snapshot_name           = "${var.stack}-${var.name}-final-snapshot-${replace(timestamp(), ":", "-")}"
  db_security_group_name        = join("-", [var.stack, var.name, "rds", "sg"])
  db_security_group_description = "Security group for the ${var.name} DB"
  engine_version_major          = replace(var.engine_version, "/(.+?)\\.(.+?)\\.*(.+)?/", "$1")
  engine_version_major_minor    = replace(var.engine_version, "/(.+?)\\.(.+?)\\..+/", "$1.$2")
  
  tags = merge(
    {
    "Name" = local.instance_name,
    },
    var.tags,
    var.rds_tags,
    )


  default_parameter_group_name = {
    mysql = "default.mysql${local.engine_version_major_minor}"

    postgres      = "default.postgres${local.engine_version_major >= 10 ? local.engine_version_major : local.engine_version_major_minor}"
    sqlserver-ee  = "default.${var.engine}${replace(var.engine_version, "/(.+?)\\.(\\d).*?\\..+/", "-$1.$2")}"
    sqlserver-se  = "default.${var.engine}${replace(var.engine_version, "/(.+?)\\.(\\d).*?\\..+/", "-$1.$2")}"
    sqlserver-ex  = "default.${var.engine}${replace(var.engine_version, "/(.+?)\\.(\\d).*?\\..+/", "-$1.$2")}"
    sqlserver-web = "default.${var.engine}${replace(var.engine_version, "/(.+?)\\.(\\d).*?\\..+/", "-$1.$2")}"
  }

}

resource "aws_iam_role" "enhanced_monitoring_role" {

  count = var.monitoring_interval > 0 && var.monitoring_role_arn == null ? 1 : 0

  name               = local.monitoring_role_name
  path               = var.monitoring_role_arn_path
  assume_role_policy = data.aws_iam_policy_document.enhanced_monitoring_role.json
  tags = merge(
  {
  "Name" = local.instance_name,
  },
  var.tags,
  var.rds_tags,
  )
}

data "aws_iam_policy_document" "enhanced_monitoring_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["monitoring.rds.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "enhanced_monitoring_role_attachment" {
  count      = var.monitoring_interval > 0 && var.monitoring_role_arn == null ? 1 : 0
  depends_on = [aws_iam_role.enhanced_monitoring_role]
  role = element(
    concat(aws_iam_role.enhanced_monitoring_role.*.name, [""]),
    0,
  )
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
}

data "template_file" "auto_created_monitoring_role_arn" {
  template = var.monitoring_interval > 0 ? element(concat(aws_iam_role.enhanced_monitoring_role.*.arn, [""]), 0) : ""
}

data "aws_vpc" "selected" {
  filter {
    name   = "tag:Name"
    values = [join("-", [var.stack, "vpc"])]
  }
}

data "aws_subnet_ids" "private_db" {
  vpc_id = data.aws_vpc.selected.id
  filter {
    name   = "tag:Name"
    values = ["*-private-*"]
  }
}

data "aws_subnet" "private_db" {
  for_each = data.aws_subnet_ids.private_db.ids
  id       = each.value
}

resource "aws_db_subnet_group" "db" {
  count = var.create_subnet_group ? 1 : 0

  name        = local.db_subnet_group_name
  description = local.db_subnet_group_description
  subnet_ids  = data.aws_subnet_ids.private_db.ids
  tags = merge(
  {
  "Name" = local.db_subnet_group_name,
  },
  var.tags,
  var.rds_tags,
  )
}

# ------------------------------------------------------------------------------
# CREATE THE SECURITY GROUP THAT CONTROLS WHAT TRAFFIC CAN CONNECT TO THE DB
# ------------------------------------------------------------------------------

resource "aws_security_group" "db" {
  name        = local.db_security_group_name
  description = local.db_security_group_description
  vpc_id      = data.aws_vpc.selected.id
  tags = merge(
  {
  "Name" = join("-", [var.stack, var.name, "rds", "sg"]),
  },
  var.tags,
  var.rds_tags,
  )
}

resource "aws_security_group_rule" "inbound_rule" {
  count             = signum(length("${values(data.aws_subnet.private_db).*.cidr_block}"))
  type              = "ingress"
  from_port         = var.port
  to_port           = var.port
  protocol          = "tcp"
  cidr_blocks       = "${values(data.aws_subnet.private_db).*.cidr_block}"
  security_group_id = aws_security_group.db.id
}

 resource "random_password" "db_password" {
   length  = 16
   special = false
 }

 resource "aws_secretsmanager_secret" "db_credentials" {
   name = "${aws_db_instance.master.name}-credentials"
 }

 resource "aws_secretsmanager_secret_version" "db_credentials" {
   secret_id     = aws_secretsmanager_secret.db_credentials.id
   secret_string = <<EOF
 {
   "username": "${aws_db_instance.master.username}",
   "password": "${random_password.db_password.result}",
   "engine": "${aws_db_instance.master.engine}",
   "host": "${aws_db_instance.master.address}",
   "endpoint": "${aws_db_instance.master.endpoint}",
   "port": ${aws_db_instance.master.port}
 }
 EOF
 }

resource "aws_db_instance" "master" {

  name           = var.db_name
  identifier     = local.instance_name
  engine         = var.engine
  engine_version = var.engine_version
  license_model  = var.license_model == null ? data.template_file.default_license_model.rendered : var.license_model
  instance_class        = var.instance_type
  iops                  = var.iops
  allocated_storage     = var.allocated_storage
  max_allocated_storage = var.max_allocated_storage
  storage_type          = var.storage_type
  storage_encrypted     = var.storage_encrypted
  kms_key_id            = var.kms_key_arn
  port                  = var.port

  publicly_accessible = var.publicly_accessible
  vpc_security_group_ids = [aws_security_group.db.id]
  db_subnet_group_name   = local.db_subnet_group_name
  parameter_group_name   = lookup(local.default_parameter_group_name, var.engine)    

  #option_group_name      = var.option_group_name

  username = var.master_username
  password = random_password.db_password.result
  #password = var.master_password

  # password = random_password.db_password.result

  iam_database_authentication_enabled = var.iam_database_authentication_enabled

  backup_retention_period   = var.backup_retention_period
  backup_window             = var.backup_window
  final_snapshot_identifier = local.final_snapshot_name
  skip_final_snapshot       = var.skip_final_snapshot

  snapshot_identifier = var.snapshot_identifier

  apply_immediately               = var.apply_immediately
  maintenance_window              = var.maintenance_window
  monitoring_interval             = var.monitoring_interval
  monitoring_role_arn             = var.monitoring_role_arn != null ? var.monitoring_role_arn : data.template_file.auto_created_monitoring_role_arn.rendered
  enabled_cloudwatch_logs_exports = var.enabled_cloudwatch_logs_exports
  auto_minor_version_upgrade      = var.auto_minor_version_upgrade
  allow_major_version_upgrade     = var.allow_major_version_upgrade

  multi_az                        = var.multi_az
  ca_cert_identifier              = var.ca_cert_identifier

 
  tags = merge(
  {
  "Name" = local.instance_name,
  },
  var.tags,
  var.rds_tags,
  )

  copy_tags_to_snapshot = var.copy_tags_to_snapshot

  replicate_source_db = var.replicate_source_db
  deletion_protection = var.deletion_protection

  depends_on = [aws_db_subnet_group.db]

  lifecycle {
    ignore_changes = [snapshot_identifier,final_snapshot_identifier]
  }
}

data "template_file" "default_license_model" {
  template = var.default_license_models[var.engine]
}

resource "aws_ssm_parameter" "db_uri" {
  name  = "${var.ssm_parameter_path}/JDBC_URL"
  type  = "String"
  value = "${aws_db_instance.master.endpoint}/${var.db_name}"
  overwrite = true
}

resource "aws_ssm_parameter" "db_host" {
  name  = "${var.ssm_parameter_path}/DB_HOST"
  type  = "String"
  value = element(split(":", aws_db_instance.master.endpoint), 0)
  overwrite = true
}

resource "aws_ssm_parameter" "db_name" {
  name  = "${var.ssm_parameter_path}/DB_NAME"
  type  = "String"
  value = var.name
  overwrite = true
}

resource "aws_ssm_parameter" "db_port" {
  name  = "${var.ssm_parameter_path}/DB_PORT"
  type  = "String"
  value = var.port
  overwrite = true
}
