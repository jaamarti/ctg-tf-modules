# Module: vpc

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# CREATE A VPC
# This Terraform template creates a full VPC meant to run apps. The VPC includes 3 types of subnets:
# - Public (one per AZ)
# - Private-App (one per AZ)
# - Private-Peristence (one per AZ)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ---------------------------------------------------------------------------------------------------------------------
# SET TERRAFORM RUNTIME REQUIREMENTS
# ---------------------------------------------------------------------------------------------------------------------

provider "aws" {

}

terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket         = "terraform-state-lego-test"
    key            = "workspace/global/vpc/terraform.tfstate"
    region         = "us-east-2"
    # Replace this with your DynamoDB table name!
    dynamodb_table = "terraform-locks-lego"
    encrypt        = true
  }
}

data "aws_region" "current-region" {}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE VPC AND INTERNET GATEWAY
# ---------------------------------------------------------------------------------------------------------------------

# Create the VPC
resource "aws_vpc" "main" {
  cidr_block           = var.cidr_block
  instance_tenancy     = var.tenancy
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = merge(
    { Name = var.vpc_name },
    var.custom_tags,
    var.vpc_custom_tags,
  )
}

# Create an Internet Gateway for our VPC
resource "aws_internet_gateway" "main" {
  count  = var.create_public_subnets ? 1 : 0
  vpc_id = aws_vpc.main.id
  tags = merge(
    { Name = var.vpc_name },
    var.custom_tags,
  )
}

# Get a list of Availability Zones in the current region
data "aws_availability_zones" "all" {
  state            = var.availability_zone_state
  exclude_names    = var.availability_zone_exclude_names
  exclude_zone_ids = var.availability_zone_exclude_ids
}

# ---------------------------------------------------------------------------------------------------------------------
# CONFIGURE THE DEFAULT SECURITY GROUP AND NETWORK ACLS
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_default_security_group" "default" {
  count = var.enable_default_security_group ? 1 : 0

  vpc_id = aws_vpc.main.id

  dynamic "ingress" {
    for_each = var.default_security_group_ingress_rules
    content {
      from_port        = ingress.value["from_port"]
      to_port          = ingress.value["to_port"]
      protocol         = ingress.value["protocol"]
      cidr_blocks      = lookup(ingress.value, "cidr_blocks", null)
      ipv6_cidr_blocks = lookup(ingress.value, "ipv6_cidr_blocks", null)
      security_groups  = lookup(ingress.value, "security_groups", null)
      self             = lookup(ingress.value, "self", null)
      prefix_list_ids  = lookup(ingress.value, "prefix_list_ids", null)
      description      = lookup(ingress.value, "description", null)
    }
  }

  dynamic "egress" {
    for_each = var.default_security_group_egress_rules
    content {
      from_port        = egress.value["from_port"]
      to_port          = egress.value["to_port"]
      protocol         = egress.value["protocol"]
      cidr_blocks      = lookup(egress.value, "cidr_blocks", null)
      ipv6_cidr_blocks = lookup(egress.value, "ipv6_cidr_blocks", null)
      security_groups  = lookup(egress.value, "security_groups", null)
      self             = lookup(egress.value, "self", null)
      prefix_list_ids  = lookup(egress.value, "prefix_list_ids", null)
      description      = lookup(egress.value, "description", null)
    }
  }

  tags = merge(
    { Name = var.vpc_name },
    var.custom_tags,
  )
}

resource "aws_default_network_acl" "default" {
  default_network_acl_id = aws_vpc.main.default_network_acl_id
  subnet_ids = var.apply_default_nacl_rules ? concat(
    aws_subnet.public[*].id,
    aws_subnet.private-app[*].id,
    aws_subnet.private-persistence[*].id
  ) : []

  dynamic "ingress" {
    for_each = var.default_nacl_ingress_rules
    content {
      from_port       = ingress.value["from_port"]
      to_port         = ingress.value["to_port"]
      protocol        = ingress.value["protocol"]
      action          = ingress.value["action"]
      rule_no         = ingress.value["rule_no"]
      cidr_block      = lookup(ingress.value, "cidr_block", null)
      ipv6_cidr_block = lookup(ingress.value, "ipv6_cidr_block", null)
      icmp_type       = lookup(ingress.value, "icmp_type", null)
      icmp_code       = lookup(ingress.value, "icmp_code", null)
    }
  }

  dynamic "egress" {
    for_each = var.default_nacl_egress_rules
    content {
      from_port       = egress.value["from_port"]
      to_port         = egress.value["to_port"]
      protocol        = egress.value["protocol"]
      action          = egress.value["action"]
      rule_no         = egress.value["rule_no"]
      cidr_block      = lookup(egress.value, "cidr_block", null)
      ipv6_cidr_block = lookup(egress.value, "ipv6_cidr_block", null)
      icmp_type       = lookup(egress.value, "icmp_type", null)
      icmp_code       = lookup(egress.value, "icmp_code", null)
    }
  }

  tags = merge(
    { Name = var.vpc_name },
    var.custom_tags,
  )
}


# ---------------------------------------------------------------------------------------------------------------------
# CREATE PUBLIC SUBNETS
# Any resource that must be addressable from the public Internet should be placed in a Public Subnet.  E.g. ELB's, web
# servers, etc.
# ---------------------------------------------------------------------------------------------------------------------

# Create public subnets, one per Availability Zone
resource "aws_subnet" "public" {
  count             = var.create_public_subnets ? data.template_file.num_availability_zones.rendered : 0
  vpc_id            = aws_vpc.main.id
  availability_zone = element(data.aws_availability_zones.all.names, count.index)
  cidr_block = lookup(
    var.public_subnet_cidr_blocks,
    "AZ-${count.index}",
    cidrsubnet(var.cidr_block, var.public_subnet_bits, count.index),
  )
  map_public_ip_on_launch = var.map_public_ip_on_launch

  tags = merge(
    { Name = "${var.vpc_name}-public-${count.index}" },
    var.custom_tags,
    var.public_subnet_custom_tags,
  )
}

# Create a Route Table for public subnets
# - This routes all public traffic through the Internet gateway
# - All traffic to endpoints within the VPC is by default routed w/o going through the dirty Internet
resource "aws_route_table" "public" {
  count  = var.create_public_subnets ? 1 : 0
  vpc_id = aws_vpc.main.id
  tags = merge(
    { Name = "${var.vpc_name}-public" },
    var.custom_tags,
  )
}

# It's important that we define this route as a separate terraform resource and not inline in aws_route_table.public because
# otherwise Terraform will not function correctly, per the note at https://www.terraform.io/docs/providers/aws/r/route.html.
resource "aws_route" "internet" {
  count                  = var.create_public_subnets ? 1 : 0
  route_table_id         = aws_route_table.public[0].id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main[0].id

  # A workaround for a series of eventual consistency bugs in Terraform. For a list of the errors, see the related
  # bugs described in this issue: https://github.com/hashicorp/terraform/issues/8542. The workaround is based on:
  # https://github.com/hashicorp/terraform/issues/5335 and https://charity.wtf/2016/04/14/scrapbag-of-useful-terraform-tips/
  depends_on = [
    aws_internet_gateway.main,
    aws_route_table.public,
  ]

  # Workaround for https://github.com/terraform-providers/terraform-provider-aws/issues/338
  timeouts {
    create = "5m"
  }
}

# Associate each public subnet with a public route table
resource "aws_route_table_association" "public" {
  count          = var.create_public_subnets ? data.template_file.num_availability_zones.rendered : 0
  subnet_id      = element(aws_subnet.public.*.id, count.index)
  route_table_id = aws_route_table.public[0].id
}

# ---------------------------------------------------------------------------------------------------------------------
# LAUNCH THE NAT GATEWAYS
# A NAT Gateway enables instances in the private subnet to connect to the Internet or other AWS services, but prevents
# the Internet from initiating a connection to those instances.
#
# When launching a development VPC, route all traffic through a single NAT Gateway in one Availability Zone to save
# money.  When launching a production VPC, route traffic through one NAT Gateway per Availability Zone for maximum
# availability.
#
# See http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/vpc-nat-gateway.html
# ---------------------------------------------------------------------------------------------------------------------

# A NAT Gateway must be associated with an Elastic IP Address
resource "aws_eip" "nat" {
  count      = var.create_public_subnets && var.use_custom_nat_eips == false ? var.num_nat_gateways : 0
  vpc        = true
  tags       = var.custom_tags
  depends_on = [aws_internet_gateway.main]
}

resource "aws_nat_gateway" "nat" {
  count         = var.create_public_subnets ? var.num_nat_gateways : 0
  allocation_id = element(local.nat_eips, count.index)
  subnet_id     = element(aws_subnet.public.*.id, count.index)
  tags = merge(
    { Name = "${var.vpc_name}-nat-gateway-${count.index}" },
    var.custom_tags,
    var.nat_gateway_custom_tags,
  )

  # As recommended by the Terraform docs: https://www.terraform.io/docs/providers/aws/r/nat_gateway.html
  depends_on = [aws_internet_gateway.main]
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE PRIVATE SUBNETS AT THE "APP" TIER
# These subnets are private and meant to house any application/service that does not require direct connectivity from
# users.  Includes app servers, queue processors, reporting systems, etc.
# ---------------------------------------------------------------------------------------------------------------------

# Create a private subnets per AZ for our "App" tier
resource "aws_subnet" "private-app" {
  count             = var.create_private_app_subnets ? data.template_file.num_availability_zones.rendered : 0
  vpc_id            = aws_vpc.main.id
  availability_zone = element(data.aws_availability_zones.all.names, count.index)
  cidr_block = lookup(
    var.private_app_subnet_cidr_blocks,
    "AZ-${count.index}",
    cidrsubnet(var.cidr_block, var.private_subnet_bits, count.index + local.private_spacing),
  )
  tags = merge(
    { Name = "${var.vpc_name}-private-app-${count.index}" },
    var.custom_tags,
    var.private_app_subnet_custom_tags,
  )
}

# Create a Route Table for each private app subnet
# - All traffic to endpoints within the subnet to which this is attached will be enabled by default
# - For all non-VPC traffic (i.e. public Internet) traffic, we'll route this to the NAT instance for this Availability
#   Zone.
# Note that we need not specify any routing rules for this here because our HA NAT Instance will automatically update
# the Route Table upon booting.
resource "aws_route_table" "private-app" {
  count  = var.create_private_app_subnets ? data.template_file.num_availability_zones.rendered : 0
  vpc_id = aws_vpc.main.id

  propagating_vgws = var.private_propagating_vgws

  tags = merge(
    { Name = "${var.vpc_name}-private-app-${count.index}" },
    var.custom_tags,
  )
}

# Create a route for outbound Internet traffic.
resource "aws_route" "nat" {
  count                  = var.num_nat_gateways == 0 || var.create_private_app_subnets == false || var.create_public_subnets == false ? 0 : data.template_file.num_availability_zones.rendered
  route_table_id         = element(aws_route_table.private-app.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = element(aws_nat_gateway.nat.*.id, count.index)

  # A workaround for a series of eventual consistency bugs in Terraform. For a list of the errors, see the related
  # bugs described in this issue: https://github.com/hashicorp/terraform/issues/8542. The workaround is based on:
  # https://github.com/hashicorp/terraform/issues/5335 and https://charity.wtf/2016/04/14/scrapbag-of-useful-terraform-tips/
  depends_on = [
    aws_internet_gateway.main,
    aws_route_table.private-app,
  ]

  # Workaround for https://github.com/terraform-providers/terraform-provider-aws/issues/338
  timeouts {
    create = "5m"
  }
}

# Associate each private-app subnet with its respective route table
resource "aws_route_table_association" "private-app" {
  count          = var.create_private_app_subnets ? data.template_file.num_availability_zones.rendered : 0
  subnet_id      = element(aws_subnet.private-app.*.id, count.index)
  route_table_id = element(aws_route_table.private-app.*.id, count.index)
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE PRIVATE SUBNETS AT THE "PERSISTENCE" TIER
# These subnets are private and meant to house any peristence resources. This includes Relational Databases, Cache,
# NoSQL Databases, etc.
# ---------------------------------------------------------------------------------------------------------------------

# Create one private subnet per AZ for our "Persistence" tier
resource "aws_subnet" "private-persistence" {
  count             = var.create_private_persistence_subnets ? data.template_file.num_availability_zones.rendered : 0
  vpc_id            = aws_vpc.main.id
  availability_zone = element(data.aws_availability_zones.all.names, count.index)
  cidr_block = lookup(
    var.private_persistence_subnet_cidr_blocks,
    "AZ-${count.index}",
    cidrsubnet(var.cidr_block, var.persistence_subnet_bits, count.index + local.persistence_spacing),
  )
  tags = merge(
    { Name = "${var.vpc_name}-private-persistence-${count.index}" },
    var.custom_tags,
    var.private_persistence_subnet_custom_tags,
  )
}

# Create a Route Table for each private persistence subnet
# - All traffic to endpoints within the subnet to which this is attached will be enabled by default
# - No public Internet traffic is permitted, in or out.
resource "aws_route_table" "private-persistence" {
  count  = var.create_private_persistence_subnets ? data.template_file.num_availability_zones.rendered : 0
  vpc_id = aws_vpc.main.id

  propagating_vgws = var.persistence_propagating_vgws

  tags = merge(
    { Name = "${var.vpc_name}-private-persistence-${count.index}" },
    var.custom_tags,
  )
}

# Create a route for outbound Internet traffic.
resource "aws_route" "private_persistence_nat" {
  count                  = var.create_private_persistence_subnets && var.create_public_subnets && var.allow_private_persistence_internet_access && var.num_nat_gateways > 0 ? data.template_file.num_availability_zones.rendered : 0
  route_table_id         = element(aws_route_table.private-persistence.*.id, count.index)
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = element(aws_nat_gateway.nat.*.id, count.index)

  # A workaround for a series of eventual consistency bugs in Terraform. For a list of the errors, see the related
  # bugs described in this issue: https://github.com/hashicorp/terraform/issues/8542. The workaround is based on:
  # https://github.com/hashicorp/terraform/issues/5335 and https://charity.wtf/2016/04/14/scrapbag-of-useful-terraform-tips/
  depends_on = [
    aws_internet_gateway.main,
    aws_route_table.private-persistence,
  ]

  # Workaround for https://github.com/terraform-providers/terraform-provider-aws/issues/338
  timeouts {
    create = "5m"
  }
}

# Associate each private-persistence subnet with its respective route table
resource "aws_route_table_association" "private-persistence" {
  count          = var.create_private_persistence_subnets ? data.template_file.num_availability_zones.rendered : 0
  subnet_id      = element(aws_subnet.private-persistence.*.id, count.index)
  route_table_id = element(aws_route_table.private-persistence.*.id, count.index)
}

# ---------------------------------------------------------------------------------------------------------------------
# SETUP VPC ENDPOINTS
# This ensures that all requests to the AWS API for S3 and DynamoDB are routed through the VPC instead of the Public
# Internet. We use aws_vpc_endpoint_route_table_association resources rather than associating route tables directly
# in the aws_vpc_endpoint resource to avoid dependency errors when modifying route tables.
# See: https://github.com/gruntwork-io/terraform-aws-vpc/pull/89
#      https://github.com/gruntwork-io/terraform-aws-vpc/issues/49
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_vpc_endpoint" "s3" {
  count        = var.create_vpc_endpoints ? 1 : 0
  vpc_id       = aws_vpc.main.id
  service_name = "com.amazonaws.${var.aws_region}.s3"
  tags         = var.custom_tags
}

resource "aws_vpc_endpoint_route_table_association" "s3_public" {
  count           = var.create_vpc_endpoints && var.create_public_subnets ? 1 : 0
  vpc_endpoint_id = aws_vpc_endpoint.s3[0].id
  route_table_id  = aws_route_table.public[0].id
}

resource "aws_vpc_endpoint_route_table_association" "s3_private" {
  count = var.create_vpc_endpoints && var.create_private_app_subnets ? length(aws_route_table.private-app.*.id) : 0

  vpc_endpoint_id = aws_vpc_endpoint.s3[0].id
  route_table_id  = element(aws_route_table.private-app.*.id, count.index)
}

resource "aws_vpc_endpoint_route_table_association" "s3_persistence" {
  count = var.create_vpc_endpoints && var.create_private_persistence_subnets ? length(aws_route_table.private-persistence.*.id) : 0

  vpc_endpoint_id = aws_vpc_endpoint.s3[0].id
  route_table_id  = element(aws_route_table.private-persistence.*.id, count.index)
}

resource "aws_vpc_endpoint" "dynamodb" {
  count        = var.create_vpc_endpoints ? 1 : 0
  vpc_id       = aws_vpc.main.id
  service_name = "com.amazonaws.${var.aws_region}.dynamodb"
  tags         = var.custom_tags
}

resource "aws_vpc_endpoint_route_table_association" "dynamodb_public" {
  count           = var.create_vpc_endpoints && var.create_public_subnets ? 1 : 0
  vpc_endpoint_id = aws_vpc_endpoint.dynamodb[0].id
  route_table_id  = aws_route_table.public[0].id
}

resource "aws_vpc_endpoint_route_table_association" "dynamodb_private" {
  count = var.create_vpc_endpoints && var.create_private_app_subnets ? length(aws_route_table.private-app.*.id) : 0

  vpc_endpoint_id = aws_vpc_endpoint.dynamodb[0].id
  route_table_id  = element(aws_route_table.private-app.*.id, count.index)
}

resource "aws_vpc_endpoint_route_table_association" "dynamodb_persistence" {
  count = var.create_vpc_endpoints && var.create_private_persistence_subnets ? length(aws_route_table.private-persistence.*.id) : 0

  vpc_endpoint_id = aws_vpc_endpoint.dynamodb[0].id
  route_table_id  = element(aws_route_table.private-persistence.*.id, count.index)
}


# ---------------------------------------------------------------------------------------------------------------------
# USE A NULL RESOURCE TO INDICATE THAT THE VPC HAS FINISHED CREATING
# Other resources can depend on this one to make sure they don't create anything in the VPC before it's ready. This
# can help to work around a Terraform or AWS issue where trying to create certain resources, such as Network ACLs,
# before the VPC's Gateway and NATs are ready, leads to a huge variety of eventual consistency bugs.
# ---------------------------------------------------------------------------------------------------------------------

resource "null_resource" "vpc_ready" {
  depends_on = [
    aws_internet_gateway.main,
    aws_nat_gateway.nat,
    aws_route.internet,
    aws_route.nat,
  ]
}

# ---------------------------------------------------------------------------------------------------------------------
# CONVENIENCE VARIABLES
# ---------------------------------------------------------------------------------------------------------------------

data "template_file" "num_availability_zones" {
  template = (
    var.num_availability_zones == null
    ? length(data.aws_availability_zones.all.names)
    : min(var.num_availability_zones, length(data.aws_availability_zones.all.names))
  )
}

locals {
  private_spacing     = var.private_subnet_spacing != null ? var.private_subnet_spacing : var.subnet_spacing
  persistence_spacing = var.persistence_subnet_spacing != null ? var.persistence_subnet_spacing : 2 * var.subnet_spacing
  nat_eips            = var.use_custom_nat_eips ? var.custom_nat_eips : aws_eip.nat[*].id
}
