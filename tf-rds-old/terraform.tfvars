vpc_name            =       ["comsearch"]
allocated_storage   =       10
allow_connections_from_bastion_host     =       "true"
apply_immediately     =       "false"
backup_retention_period = 30
enable_perf_alarms  =       "false"
engine              =       "postgres"
engine_version      =       "12.7"
environment         =       "development"
high_cpu_utilization_period     =       600
high_cpu_utilization_threshold  =       90
instance_type       =       "db.t2.micro" 
low_disk_space_available_period     =       600
low_disk_space_available_threshold  =       20
low_memory_available_period     =       600
low_memory_available_threshold  =       20
master_password     =       "Administrat0r2021"
master_username     =       "postresadmin"
multi_az            =       "true"
name                =       "dev-db"
port                =       "5432"
ssm_parameter_path  =       "/path"


