package test

import (
	"testing"
	"github.com/gruntwork-io/terratest/modules/terraform"
 	// "fmt"
    // "net"
	"log"
	 "os"
	 "path/filepath"
	//  "strings"

	//  "github.com/gruntwork-io/terratest/modules/aws"
	//  "github.com/gruntwork-io/terratest/modules/random"
	 "github.com/stretchr/testify/assert"
)

func TestRds(t *testing.T) {
	t.Parallel()


		var dir string
		dir, err := os.Getwd()
		if err != nil {
			log.Fatal(err)
		}
	
	var modules = []struct {
		modName     string
		templatePath string
		tfvarsFile   string
		withLogs     bool
	}{
		{
			"sns",
			"../",
			"terraform.tfvars",
			true,
		},
	}

	for _, testCase := range modules {

		testCase := testCase

        workingDir := filepath.Join(dir, testCase.templatePath)
	    tfvarfilePath := filepath.Join(workingDir, testCase.tfvarsFile)
		// awsRegion := os.Getenv("AWS_DEFAULT_REGION")

		TerraformOptions := &terraform.Options{
			// Source path of Terraform directory.
//			TerraformDir: testCase.templatePath,
			TerraformDir: workingDir,
			Upgrade: true,
			VarFiles: []string{tfvarfilePath},
			Vars: map[string]interface{}{},             
		}
      

		terraform.InitAndApply(t, TerraformOptions)

		defer terraform.Destroy(t, TerraformOptions)

		//////////////// Testing sections ///////////////////////////////

		if (testCase.modName == "sns") {
			snsTopicArn := terraform.Output(t, TerraformOptions, "arn")
			assert.NotNil(t, snsTopicArn)
		}
		///////////////////////////////////////////////////////////////////

	}

}