# vpc

**Original Creator:**  Vladimir Rudyk

**Support:**  devops

**Support_email:**  vladimir.rudyk@commscope.com

Deploys vpc resources to create a new env for Comsearch

  # 3-Tier VPC

This directory creates a 3-Tier [Virtual Private Cloud (VPC)](https://aws.amazon.com/vpc/) that can be used for either
production or non-production workloads.

The resources that are created include:

1. The VPC itself.
1. Subnets, which are isolated subdivisions within the VPC. There are 3 "tiers" of subnets: public, private app, and
   private persistence.
1. Route tables, which provide routing rules for the subnets.
1. Internet Gateways to route traffic to the public Internet from public subnets.
1. NATs to route traffic to the public Internet from private subnets.
1. Network ACLs that control what traffic can go in and out of each subnet.
1. VPC Peering connection that allows limited access from the Mgmt VPC.

Under the hood, this is all implemented using Terraform modules from the Gruntwork
[module-vpc](https://github.com/gruntwork-io/module-vpc) repo. If you don't have access to this repo, email
support@gruntwork.io.

## Core concepts

To understand core concepts like what's a VPC, how subnets are configured, how network ACLs work, and more, see the
documentation in the [module-vpc](https://github.com/gruntwork-io/module-vpc) repo.


## Services Created:

## Variables: 
(examples can be found in testing.tfvars or variables.tf) 
#### Required:

aws_region          -  The AWS Region where this VPC will exist. 
vpc_name            -  Name of the VPC. Examples include 'prod', 'dev', 'mgmt', etc.
cidr_block          -  The IP address range of the VPC in CIDR notation. A prefix of /16 is recommended. Do not use a prefix higher than /27. Examples include '10.100.0.0/)

num_nat_gateways    -  (The number of NAT Gateways to launch for this VPC. For production VPCs, a NAT Gateway should be placed in each Availability Zone (so likely 3 total), whereas for non-prod VPCs, just one Availability Zone (and hence 1 NAT Gateway) will suffice.)


#### Optional:

num_availability_zones        - How many AWS Availability Zones (AZs) to use. One subnet of each type (public, private app, private persistence) will be created in each AZ. All AZs will be used if you provide a value that is more than the number of AZs in a region. A value of null means all AZs should be used. For example, if you specify 3 in a region with 5 AZs, subnets will be created in just 3 AZs instead of all 5. On the other hand, if you specify 6 in the same region, all 5 AZs will be used with no duplicates (same as setting this to 5) (default     = null)


availability_zone_exclude_names             - List of excluded Availability Zone names. (default     = [])


availability_zone_exclude_ids               - List of excluded Availability Zone IDs. (default     = [])

availability_zone_state                     - Allows to filter list of Availability Zones based on their current state. Can be either \"available\", \"information\", \"impaired\" or \"unavailable\". By default the list includes a complete set of Availability Zones to which the underlying AWS account has access, regardless of their state. (default     = null)


allow_private_persistence_internet_access  - Should the private persistence subnet be allowed outbound access to the internet. (default     = false)


use_custom_nat_eips                        -  Set to true to use existing EIPs, passed in via var.custom_nat_eips, for the NAT gateway(s), instead of creating new ones. (default     = false)


custom_nat_eips                           -  ("The list of EIPs (allocation ids) to use for the NAT gateways. Their number has to match the one given in 'num_nat_gateways'. Must be set if var.use_custom_nat_eips us true. (default     = [])


public_subnet_cidr_blocks                 - A map listing the specific CIDR blocks desired for each public subnet. The key must be in the form AZ-0, AZ-1, ... AZ-n where n is the number of Availability Zones. If left blank, we will compute a reasonable CIDR block for each subnet. (default     = {})


private_app_subnet_cidr_blocks            - A map listing the specific CIDR blocks desired for each private-app subnet. The key must be in the form AZ-0, AZ-1, ... AZ-n where n is the number of Availability Zones. If left blank, we will compute a reasonable CIDR block for each subnet. (default     = {})


private_persistence_subnet_cidr_blocks    - A map listing the specific CIDR blocks desired for each private-persistence subnet. The key must be in the form AZ-0, AZ-1, ... AZ-n where n is the number of Availability Zones. If left blank, we will compute a reasonable CIDR block for each subnet.   (default     = {})
  
private_propagating_vgws                  - A list of Virtual Gateways that will propagate routes to private subnets. All routes from VPN connections that use Virtual Gateways listed here will appear in route tables of private subnets. If left empty, no routes will be propagated. (default     = [])

persistence_propagating_vgws              - A list of Virtual Gateways that will propagate routes to persistence subnets. All routes from VPN connections that use Virtual Gateways listed here will appear in route tables of persistence subnets. If left empty, no routes will be propagated. (default     = [])


tenancy                                   - The allowed tenancy of instances launched into the selected VPC. Must be one of: default, dedicated, or host. (default     = "default")


custom_tags                               - A map of tags to apply to the VPC, Subnets, Route Tables, Internet Gateway, default security group, and default NACLs. The key is the tag name and the value is the tag value. Note that the tag 'Name' is automatically added by this module but may be optionally overwritten by this variable. (default     = {})


vpc_custom_tags                           - A map of tags to apply just to the VPC itself, but not any of the other resources. The key is the tag name and the value is the tag value. Note that tags defined here will override tags defined as custom_tags in case of conflict. (default     = {})


public_subnet_custom_tags                 - A map of tags to apply to the public Subnet, on top of the custom_tags. The key is the tag name and the value is the tag value. Note that tags defined here will override tags defined as custom_tags in case of conflict. (default     = {})


private_app_subnet_custom_tags            - A map of tags to apply to the private-app Subnet, on top of the custom_tags. The key is the tag name and the value is the tag value. Note that tags defined here will override tags defined as custom_tags in case of conflict. (default     = {})


private_persistence_subnet_custom_tags    - (A map of tags to apply to the private-persistence Subnet, on top of the custom_tags. The key is the tag name and the value is the tag value. Note that tags defined here will override tags defined as custom_tags in case of conflict.  (default     = {})


nat_gateway_custom_tags                   - A map of tags to apply to the NAT gateways, on top of the custom_tags. The key is the tag name and the value is the tag value. Note that tags defined here will override tags defined as custom_tags in case of conflict. (default     = {})

subnet_spacing                            - The amount of spacing between the different subnet types. (default     = 10)

private_subnet_spacing                    - The amount of spacing between private app subnets. (default     = null)

persistence_subnet_spacing                - The amount of spacing between the private persistence subnets. Default: 2 times the value of private_subnet_spacing. (default     = null)

public_subnet_bits                        - Takes the CIDR prefix and adds these many bits to it for calculating subnet ranges.  MAKE SURE if you change this you also change the CIDR spacing or you may hit       errors.  See cidrsubnet interpolation in terraform config for more information. (default     = 5)

private_subnet_bits                       - Takes the CIDR prefix and adds these many bits to it for calculating subnet ranges.  MAKE SURE if you change this you also change the CIDR spacing or you may hit errors.  See cidrsubnet interpolation in terraform config for more information. (default     = 5)

persistence_subnet_bits.                  - Takes the CIDR prefix and adds these many bits to it for calculating subnet ranges.  MAKE SURE if you change this you also change the CIDR spacing or you may hit errors.  See cidrsubnet interpolation in terraform config for more information. (default     = 5)

map_public_ip_on_launch                   - Specify true to indicate that instances launched into the public subnet should be assigned a public IP address (versus a private IP address). (default     = false)

create_vpc_endpoints                      - Create VPC endpoints for S3 and DynamoDB.  (default     = true)

create_public_subnets                     -  If set to false, this module will NOT create the public subnet tier. This is useful for VPCs that only need private subnets. Note that setting this to false also means the module will NOT create an Internet Gateway or the NAT gateways, so if you want any public Internet access in the VPC (even outbound access—e.g., to run apt get), you'll need to provide it yourself via some other mechanism (e.g., via VPC peering, a Transit Gateway, Direct Connect, etc). (default     = true)

create_private_app_subnets                - If set to false, this module will NOT create the private app subnet tier. (default     = true)

create_private_persistence_subnets        - If set to false, this module will NOT create the private persistence subnet tier. (default     = true)


enable_default_security_group             -If set to false, the default security groups will NOT be created. This variable is a workaround to a terraform limitation where overriding var.default_security_group_ingress_rules = {} and var.default_security_group_egress_rules = {} does not remove the rules. More information at: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/default_security_group#removing-aws_default_security_group-from-your-configuration.  (default     = true)

default_security_group_ingress_rules      - The ingress rules to apply to the default security group in the VPC. This is the security group that is used by any resource that doesn't have its own security group attached. The value for this variable must be a map where the keys are a unique name for each rule and the values are objects with the same fields as the ingress block in the aws_default_security_group resource: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/default_security_group#ingress-block.   

default = {

    AllowAllFromSelf = {
      from_port = 0
      to_port   = 0
      protocol  = "-1"
      self      = true
    }
  }
}

"default_security_group_egress_rules" (The egress rules to apply to the default security group in the VPC. This is the security group that is used by any resource that doesn't have its own security group attached. The value for this variable must be a map where the keys are a unique name for each rule and the values are objects with the same fields as the egress block in the aws_default_security_group resource: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/default_security_group#egress-block)

default = {
    # The default AWS configures:
    # https://docs.aws.amazon.com/vpc/latest/userguide/VPC_SecurityGroups.html#DefaultSecurityGroup
    AllowAllOutbound = {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }
  }
}

"apply_default_nacl_rules"  (If true, will apply the default NACL rules in var.default_nacl_ingress_rules and var.default_nacl_egress_rules to the public, private, and persistence subnets created by this module. If false, the default NACL rules will still be managed by this module, but they will not be applied to any subnets. If you are managing NACLs for the subnets using another module or for some reason do not want to use the default NACLs, set this to false)
(default     = false)

"default_nacl_ingress_rules"  (The ingress rules to apply to the default NACL in the VPC. This is the NACL that is used by any subnet that doesn't have its own NACL attached. The value for this variable must be a map where the keys are a unique name for each rule and the values are objects with the same fields as the ingress block in the aws_default_network_acl resource: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/default_network_acl)
default = {
    # The default AWS configures:
    # https://docs.aws.amazon.com/vpc/latest/userguide/vpc-network-acls.html#default-network-acl
    AllowAll = {
      from_port  = 0
      to_port    = 0
      action     = "allow"
      protocol   = "-1"
      cidr_block = "0.0.0.0/0"
      rule_no    = 100
    }
  }
}

"default_nacl_egress_rules"   (The egress rules to apply to the default NACL in the VPC. This is the security group that is used by any subnet that doesn't have its own NACL attached. The value for this variable must be a map where the keys are a unique name for each rule and the values are objects with the same fields as the egress block in the aws_default_network_acl resource: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/default_network_acl."
default = {
    # The default AWS configures:
    # https://docs.aws.amazon.com/vpc/latest/userguide/vpc-network-acls.html#default-network-acl
    AllowAll = {
      from_port  = 0
      to_port    = 0
      action     = "allow"
      protocol   = "-1"
      cidr_block = "0.0.0.0/0"
      rule_no    = 100
    }
  }
}
S


## pre-commit

* Install the pre-commit package

```
brew install pre-commit``

* Install the pre-commit hook

```
pre-commit install
```

## Terratest


1. Verify you have the latest version of Go

2. Change into terratest dir with command:

cd terratest

3. Then run command:

go test -v -timeout 45m

4. For run an specific fuction can use:

go test -v -run <function_name_here>




![Run terraform graph | dot -Tsvg > dynamodb-graph.svg](./dynamodb-graph.svg)

